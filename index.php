<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PHP Activity 2</title>
</head>
<body>
	<h2>Divisible by 5</h2>
	<?php

	for($number=1; $number <= 500; $number++){
		if ($number % 5 == 0 ){
			echo "$number, ";
		}
	}
	?>

	<h2>Array Manipulation</h2>
	<?php array_push($students, 'Jet Lee') ?>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>

	<?php array_push($students, 'Jackie Chan') ?>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>

	<?php array_shift($students) ?>
	<pre><?php print_r($students) ?></pre>
	<pre><?php echo count($students) ?></pre>
</body>
</html>
